//
//  swiftui_apple_frameworks_ios_15App.swift
//  swiftui-apple-frameworks-ios-15
//
//  Created by JP on 21-08-23.
//

import SwiftUI

@main
struct AppleFrameworksApp: App {
    var body: some Scene {
        WindowGroup {
            FrameworkGridView()
        }
    }
}
