//
//  FrameworkGridView.swift
//  swiftui-apple-frameworks-ios-15
//
//  Created by JP on 21-08-23.
//

import SwiftUI

struct FrameworkGridView: View {
    @StateObject var viewModel = FrameworkGridViewModel()
    
    var body: some View {
        NavigationView {
            ScrollView {
                LazyVGrid(columns: viewModel.columns) {
                    ForEach(MockData.frameworks) { framework in
                        FrameworkTitleView(framework: framework)
                            .onTapGesture {
                                viewModel.selectedFramework = framework
                            }
                    }
                }
            }
            
            /* Example of List with Navigation
             List {
                ForEach(MockData.frameworks) { framework in
                    NavigationLink(destination: FrameworkDetailView(viewModel: FrameworkDetailViewModel(
                        framework: framework,
                        isShowingDetailView: $viewModel.isShowingDetailView))) {
                            FrameworkTitleView(framework: framework)
                        }
                }
            }*/
            .navigationTitle("Apple Frameworks")
            .sheet(isPresented: $viewModel.isShowingDetailView) {
                FrameworkDetailView(viewModel: FrameworkDetailViewModel(
                    framework: viewModel.selectedFramework!, // MVVM weird things
                    isShowingDetailView: $viewModel.isShowingDetailView
                ))
            }
        }
        .accentColor(Color(.label))
    }
}

struct FrameworkGridView_Previews: PreviewProvider {
    static var previews: some View {
        FrameworkGridView()
    }
}
