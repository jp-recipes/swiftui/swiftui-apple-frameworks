//
//  FrameworkDetailView.swift
//  swiftui-apple-frameworks-ios-15
//
//  Created by JP on 21-08-23.
//

import SwiftUI

struct FrameworkDetailView: View {
    @ObservedObject var viewModel: FrameworkDetailViewModel

    var body: some View {
        VStack {
            XDismissButton(isShowingDetailView: $viewModel.isShowingDetailView.wrappedValue)
            
            Spacer()
            
            FrameworkTitleView(framework: viewModel.framework)
            
            Text(viewModel.framework.description)
                .font(.body)
                .padding()
            
            Spacer()
            
            Link(destination: URL(string: viewModel.framework.urlString) ?? URL(string: "www.apple.com")!) {
                Label("Learn More", systemImage: "book.fill")
            }
            .buttonStyle(.bordered)
            .controlSize(.large)
            .tint(.red)
            
            
            /*Button {
                viewModel.isShowingSafariView = true
            } label: {
                // Before iOS 15 Button Styles
                //AFButton(title: "Learn More")
                Label("Learn More", systemImage: "book.fill")
            }
            .buttonStyle(.bordered)
            .controlSize(.large)
            .tint(.red)
             */
        }
        /*.fullScreenCover(isPresented: $viewModel.isShowingSafariView) {
            // Before iOS 14
            SafariView(url: URL(string: viewModel.framework.urlString)!)
        }*/
    }
}

struct FrameworkDetailView_Previews: PreviewProvider {
    static var previews: some View {
        FrameworkDetailView(viewModel: FrameworkDetailViewModel(
            framework: MockData.sampleFramework,
            isShowingDetailView: .constant(false)
        ))
    }
}
